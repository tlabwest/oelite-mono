# Based on the Gumstix Yocto distribution

DESCRIPTION = "Standalone web server written in C# that can be used to run ASP.NET applications"
SECTION = "devel/mono"
DEPENDS = "native:mono native:mono-dev mono mono-dev"
RDEPENDS_${PN} = "mono"
LICENSE = "MIT"

inherit autotools pkgconfig

RECIPE_TYPES = "machine native"

SRC_URI = "\
    "

S = "${SRCDIR}/xsp-${PV}"

PACKAGES += "${PN}-test \
	${PN}-unittest \
"

EXTRA_OECONF += "--with-runtime=/usr/bin/mono"

do_configure[prefuncs] += "do_configure_pre"
do_configure_pre() {
    echo "libdir = ${libdir}, bindir = ${bindir} "
    # fix staged binaries that point to /bin/mono
    for file in ${STAGE_DIR}/native/bin/*; do
        sed -e "s|exec /bin/mono |exec ${STAGE_DIR}/native/bin/mono |" -i $file
        sed -e "s| /lib/mono| ${STAGE_DIR}/native/lib/mono|" -i $file
    done
}

EXTRA_OEMAKE="MONO_PATH=${STAGE_DIR}/native/lib/mono/4.5"

do_install[postfuncs] += "do_install_fix_xsp4"
do_install_fix_xsp4() {
    :
    #install -d ${D}${libdir}/mono/4.5
    #cp ${D}${libdir}/mono/4.0/xsp4.exe ${D}${libdir}/mono/4.5
    #sed -e 's|/usr/lib/mono/4.0/xsp4.exe|/usr/lib/mono/4.5/xsp4.exe|' -i ${D}${bindir}/xsp4
    #sed -e 's|/usr/lib/mono/2.0/xsp2.exe|/usr/lib/mono/4.5/xsp4.exe|' -i ${D}${bindir}/xsp
}

FILES_${PN}-test = "${libdir}/xsp/test/*"

FILES_${PN}-unittest = "${libdir}/xsp/unittests/*"

FILES_${PN}-doc += "${libdir}/monodoc/*"

# AJL - For now we are going to parcel up the different XSP hosting (mod_mono, fastcgi, xsp) together. More granular packages in future
FILES_${PN} = "${bindir}/* \
	${libdir}/mono/* \
	${libdir}/xsp/4.0/* \
"

FILES_${PN}-2.0 = " ${libdir}/xsp/2.0/* \
    "

PACKAGES += "${PN}-2.0"

# We seem to run into problems with make parallelisation building mono-xsp, so disable it for now
PARALLEL_MAKE = ""

